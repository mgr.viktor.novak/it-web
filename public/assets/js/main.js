$(function () {
    "use strict";

    // Spracovanie eventu kliknutia na odkaz v záhlaví alebo v päte stránky
    $('body').on('click', '.header-menu a, .footer-menu a', function (e) {
        e.preventDefault();

        // Výpis do konzoly prehliadača
        console.log(e.target.hash);

        $('html, body').animate({
            scrollTop: (e.target.hash) ? $('a[name="' + e.target.hash.replace('#', '') + '"]').offset().top : 0
        }, 1000);
    });

    // Implementácia OwlCarousel pluginu
    $('.cards').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:3
            }
        }
    });

    // Ciele preklikávanie
    let goals = $('#goals');
    if (goals.length > 0) {
        goals.on('click', '.goal-dots span', function (e) {
            e.preventDefault();

            let target = $(this);
            if (!target.hasClass('active')) {

                $.ajax({
                    url: '/goal',
                    data: {
                        id: target.data('id')
                    },
                    dataType: 'html',
                    beforeSend: function () {
                        goals.find('.goals').addClass('loading');
                    },
                    success: function (response) {
                        goals.find('.goals').html(response);
                        goals.find('.goal-dots span').removeClass('active');
                        target.addClass('active');
                    },
                    complete: function () {
                        goals.find('.goals').removeClass('loading');
                    },
                });
            }
        });
    }

});