<?php

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Pomocná funkcia ktorá načíta dáta pre layout
 * Tieto dáta sa opakujú na každej postránke
 * @param $container
 * @param $args
 * @return mixed
 */
function layoutData($container, $args) {
    $stmt = $container->get('db')->prepare('SELECT * FROM web_text WHERE web_show = 1 ORDER BY web_order ASC');
    $stmt->execute();
    $args['texts'] = $stmt->fetchAll();

    return $args;
}

/**
 * Funkcia pre nastavenie routes pre Slim app
 */
return function (App $app) {
    $container = $app->getContainer();

    // Goal ajax call
    $app->get('/goal', function (Request $request, Response $response, array $args) use ($container) {
        try {

            $id = $request->getParam('id');
            if (!$id) {
                throw new Exception('Nekorektné vstupné údaje');
            }

            $stmt = $container->get('db')->prepare('SELECT description FROM web_goals WHERE id = ?');
            $stmt->execute([$id]);

            $description = $stmt->fetchColumn();
            if (!$description) {
                throw new Exception('Záznam neexistuje');
            }

            return $response->write($description);
        }
        catch (\Exception $e) {
            return $response->write($e->getMessage());
        }
    });

    // Contact static subpage
    $app->get('/kontakt', function (Request $request, Response $response, array $args) use ($container) {
        $args = layoutData($container, $args);
        return $container->get('renderer')->render($response, 'contact.phtml', $args);
    });

    // Homepage route
    $app->get('/', function (Request $request, Response $response, array $args) use ($container) {
        $args = layoutData($container, $args);

        if ($args['texts']) {
            foreach ($args['texts'] as $key => $text) {
                switch ($text['id']) {
                    case 'technology':
                        $stmt = $container->get('db')->prepare('SELECT * FROM web_technology WHERE display = 1 ORDER BY position ASC');
                        $stmt->execute();

                        $items = $stmt->fetchAll();
                        if ($items) {
                            $args['texts'][$key]['items'] = $items;
                        }

                        break;

                    case 'goals':
                        $stmt = $container->get('db')->prepare('SELECT * FROM web_goals WHERE display = 1 ORDER BY position ASC');
                        $stmt->execute();

                        $items = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        if ($items) {
                            $args['texts'][$key]['items'] = $items;
                        }
                        break;
                }
            }
        }

        return $container->get('renderer')->render($response, 'index.phtml', $args);
    });
};
